# Python routines for cmorlite

Simple cmorisation routines to standardise arbitrary model output data.
This repository aims at making various cmorisation routines as simple as possible. The standardisation can be applied to arbitrary model output data, as long as the target data structructure is perdefined. Perdefined tables are already given for the most common models like CMIP5/6, Cordes or Obs4Mip. The tool also aims as outomatic vriable conversion. At the moment liquid water based variables can be converted between height (mm, m etc) denities (kg/m^2) and fluxes (kg/m^2/s). Simple variable conversion like percent to fraction or mm to m is also supported.

## Installation
There a two ways of installing this repository:
1.  Creating a mini conda environment
2.  Direct installation using pip

### 1. Creating a mini conda environment
For this approach you don't need anything pre installed. This method will create a new mini conda environment using the latest python 3 version (3.9). You should clone this repository using git:

```bash
git clone https://gitlab.dkrz.de/freva/py-cmorlite.git ; cd py-cmorlite
```

then use the `Makefile` to deploy the conda environment:

```bash
make
```
This will create a new conda environment in `$HOME/conda_envs` and install the `py-cmorlite` there. Default python version is 3.9 while the default environment name is `cmorlite`. You can change version and environment name by setting the following environment variables:

```bash
export CONDA_ENV=yourname
export PYTHON=your.version.number
```
This will automatically install the py-cmorlite command into `~/conda_env/$CONDA_ENV/bin`. To be able to use the `py-cmorlite` command you'll have to add the `~/conda_env/$CONDA_ENV/bin` to your PATH environment. This can be achieved by adding the following line to your `~/.bash_profile` or `~/.bashrc` file (if not already done):
```bash
export PATH=$PATH:~/conda_env/$CONDA_ENV/bin
```
Here $CONDA_ENV is the environment variable you've set for the conda environment name. If you didn't set it (default) the environment name sould be cmorlite, hence the `export` command should be:
```bash
export PATH=$PATH:~/conda_env/cmorlite/bin
```
### 2. Direct Installation via pip
To run py-cmorlite you'll need python version 3.6 or higher. If you already have a python3.6+ installed and do not which to create a separate conda environment you can use pip to install the repository:

```bash
python3 -m pip install (--user) git+https://gitlab.dkrz.de/freva/py-cmorlite.git
```
This will automatically install the py-cmorlite command into `~/.local/bin/`. To be able to use the `py-cmorlite` command you'll have to add the `~/.local/bin/` to your PATH environment. This can be achieved by adding the following line to your `~/.bash_profile` or `~/.bashrc` file (if not already done):
```bash
export PATH=$PATH:~/.local/bin
```

## Prerequisites

To run the routine you will need a `JSON` or `TOML` file that contains the experiment metadata. An example for cordex data can be found below. There are several keys that are needed by the standardisation precedure. This essential keys are defined in the control varialbe file (cv). This control variable files are in `JSON` or `TOML` format and located in `cmor/PROJECT/PROJECT_CV.json` where as `PROJECT` is the type of model output structructure, like CMPI6. Please feel free to add definitions by submitting a merge request.

### Special keys
 There are three special keywords that need to be set. These are:
 - `_variable_entry_file` : The file name(s) that contain the variable and axis definitions
 - `_control_vocabulary_file` : The above mentioned control variable files
 - `_DRS_type` : DRS type that is defined in the `DRS_STRUCTURE` of your `freva` system.

 ## Application
 ### Using the Python based interface
 - You can choose to use the standard pre defined processing methods that that are shipped with the library. To do so simply use the `from_existing_preprocessor` class method of the `Standard` class:
 ```python
 from cmorlite import Standard as cs
 CMOR = cs.from_existing_preprocessor(metafile, input_directory, file_type='', glob_pattern='*c142*',
                                     overwrite=True, model='remo',out_dir=cs.scratch / 'test')
 ```
 A more detailed usage example can be found in the [Basic Usage Example Notebook](https://nbviewer.jupyter.org/urls/gitlab.dkrz.de/m300765/py-cmorlite/-/raw/master/docs/BasicUsage.ipynb)
 - You can also choose to regirster user defined processing methods to standardise any kind of output data. To do so you'll have to use the `register` method of the 1Standard1 class:
 ```python
CMOR = cs(metafile, input_directory, file_type='.nc', glob_pattern='*input',
                 overwrite=True,out_dir=cs.scratch / 'test', var_table=df)
CMOR.register(custom_method, key1=value1, key2=value2, key3=value3)
 ```
  A more detailed usage example can be found in the [Advanced Usage Example Notebook](https://nbviewer.jupyter.org/urls/gitlab.dkrz.de/m300765/py-cmorlite/-/raw/master/docs/AdvancedUsageExample.ipynb)
 ### Using the command line interface
 The routine can also be applied from the command line as a script. The only limitation is that solely pre defined processing methods can be use. But before running the command line interface you will need to load the _evaluation system_ framework (freva), for example by:

 ```bash
module load regiklim-ces
 ```
 Then you can use the `py-cmorlite` command:

 ```bash
Usage: py-cmorlite [-h] [--file-type FILE_TYPE] [--glob-pattern GLOB_PATTERN] [--overwrite] [--model MODEL] metadata input [input ...]

Cmorize a set of files

positional arguments:
  metadata              Path to json or toml meta-data file format
  input                 Input directory where to find files

optional arguments:
  -h, --help            show this help message and exit
  --file-type FILE_TYPE
                        Filetypes to look for in the folder (default: .nc)
  --glob-pattern GLOB_PATTERN
                        Glob pattern to be used when searching for files (default: *)
  --overwrite           Overwrite existing files. (default: False)
  --model MODEL         Model output type that is going to be cmorised. (default: remo)
 ```

for example:

```bash
 py-cmorlite ~/euc-0275-historical-remo_input.json /work/bmx828/integration/team/k204230/hpss/historical/exp063101/year* --file-type='.grb' --glob-pattern='e063101e_c140_*'
```
Here the <a name=metadata>~/euc-0275-historical-remo_input.json</a> file contains the global metadata for the individual experiment. Please refer to the end to this README for more information.

## Adding / Adjusting Metadata
There are two _main_ directories to be aware of if model data and / or variable names should be added to the structure.
- The first one can be found in [cmorlite/cmor](https://gitlab.dkrz.de/m300765/py-cmorlite/-/tree/master/cmorlite/cmor).
  This directory contains all possible control variable definitions and variable information. If necessary you might want
  to add additional information on any target variables, structure on projects etc to that this directories.
- The second important files are the variable translation files in csv format. These are unique for each model.
  The files contain information on code tables, variable names, descriptions and units. This information is need to
  convert variable names and units as defined in the _cmorlite/cmor_ directories. The translation files for each model
  are given in the [cmorlite/var_tables](https://gitlab.dkrz.de/m300765/py-cmorlite/-/tree/master/cmorlite/var_tables) directory.

It would be best practice to submit a merge request if you want to add changes to any of the above mentioned files/directories.

## Example Experiment Meta-data file
The above application example metioned a `JSON` or `TOML` metadata file. Such a metadata file might look the following:
The content of the [`~/euc-0275-historical-remo_input.json`](#metadata) mentioned in the example above might look like this:
```bash
{
    "#comment": "institution comes from institution_id match to the registered content in source_id controlled vocabulary - A user must register their institution and source_id",
    "#comment": "In most cases 'BE' is the appropriate entry for variant_label. The user should consult ODS-v2.1.0 (https://goo.gl/jVZsQl) before changing",
    "institution_id":               "GERICS",
    "#comment": "The following attributes must be defined by the user",
    "contact":                      "gerics-cordex@hzg.de",
    "#comment": "source, source_id, source_label, source_type, source_version_number and region comes from source_id match in controlled vocabulary registered content",
    "source_id":                    "GERICS-REMO2015",
    "source":                       "Regional Model version 2015",
    "model_id":                     "GERICS-REMO2015",
    "grid":                         "0.0275 x 0.0275 degree latitude x longitude",
    "grid_label":                   "gn",
    "nominal_resolution":           "3 km",
    "calendar":                     "gregorian",
    "product":                      "EUC-0275",
    "ensemble":                     "r1i1p1",
    "driving_experiment":           "NCC-NorESM1-M, historical, r1i1p1",
    "driving_model_id":             "NCC-NorESM1-M",
    "driving_experiment_name":      "historical",
    "version_id":                   "v1",
    "experiment_id":                "historical",
    "frequency":                    "1hr",
    "creation_date":                "2019-03-22T11:37:39Z",
    "Conventions":                  "CF-1.7",
    "project_id":                   "CORDEX",
    "modeling_realm":               "atmos",
    "initialization_method":        1,
    "physics_version":              1,
    "realization_index":            1,
    "cmor_version":                 "2.9.1",
    "#comment": "This is an optional field which can be changed by a user - CMOR will create the <outpath> defined below if it does not exist",
    "outpath":                      "./demo",
    "variant_label":                "r1i1p1",
    "mip_era":                      "CMIP6",
    "further_info_url":             "http://www.remo-rcm.de/",
    "contact":                      "gerics-cordex@hzg.de",
    "#comment": "The user may choose to define or omit/delete some or all of the following",
    "#comment":                      "",
    "history":                      "",
    "references":                   "",
    "_DRS_type":                    "cordex",
    "_variable_entry_file":         "CORDEX/CORDEX_??",
    "_control_vocabulary_file":     "CORDEX/CORDEX_CV.json",
    "_AXIS_ENTRY_FILE":             "CORDEX/CORDEX_coordinate.json",
    "_FORMULA_VAR_FILE":            "CORDEX/CORDEX_formula_terms.json",

    "#comment": "The following are CMOR internal settings and should not be edited by the user",
    "activity_id":                  "CORDEX",
    "output_file_template":         "<variable_id><frequency><source_id><variant_label><grid_label>",
    "output_path_template":         "<activity_id><institution_id><source_id><frequency><variable_id><grid_label><version>",
    "tracking_prefix":              "hdl:21.14100",
    "_history_template":            "%s, CMOR rewrote data to be consistent with <activity_id>, and <Conventions> standards",
    "_further_info_url_tmpl":       "https://furtherinfo.es-doc.org/<activity_id><institution_id><source_label><source_id><variable_id>"
}

```
