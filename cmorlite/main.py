"""Entry point to setup and run the cemorization."""
import argparse
from datetime import datetime
import logging
from pathlib import Path
import sys

import humanize

from .utils import read_cmor_table, get_filenames, units, run_cmd
from .preproc import preproc
from ._cmorize import get_cmor_structure, SCRATCH_DIR as scratch


__all__ = ('get_cmor_structure', 'get_filenames', 'preproc', 'read_cmor_table',
           'read_data_from_runinfo', 'scratch', 'units', 'run_cmd')


def parse_args(argv=None):
    """Consturct command line argument parser."""

    ap = argparse.ArgumentParser(prog='py-cmorlite',
            description="""Cmorize a set of files""",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument('metadata', metavar='metadata', type=Path,
                    help='Path to json or toml meta-data file format')
    ap.add_argument('input', metavar='input', type=Path, nargs="+",
                    help='Input directory where to find files')
    ap.add_argument('--file-type', type=str, default='.nc',
                   help='Filetypes to look for in the folder')
    ap.add_argument('--glob-pattern', type=str, default='*',
                   help='Glob pattern to be used when searching for files')
    ap.add_argument('--overwrite', action='store_true', default=False,
                   help='Overwrite existing files.')
    ap.add_argument('--model', type=str, default='remo',
                  help='Model output type that is going to be cmorised.')
    ap.add_argument('--output', type=Path, default=scratch,
                   help='Where should the ouput be written to')
    return ap.parse_args()

def read_data_from_runinfo(rundata):
    """Get all information form the run inof json file.

    Parameters:
    ===========
     run_data : str, pathlib.Path, dict
        path to the json file containing the experiment metadata

    Returns:
    ========
     dict, dict : dictionaries containing experiment information,
                  and variable target metadata information
    """

    # First the read the json file, with experiment information itself
    if isinstance(rundata, (str, Path)):
        rundata = read_cmor_table(rundata, use_cmor_dir=False)
    return rundata, read_cmor_table(rundata['_variable_entry_file'])

def cmd_interface():
    """Function for that serves as entry point."""

    args = parse_args(sys.argv)
    rundata, table = read_data_from_runinfo(args.metadata)
    config = dict(
            rundata=rundata,
            cmortable=table,
            drstype=get_cmor_structure(rundata['_DRS_type']),
            files=get_filenames(args.input, args.file_type, args.glob_pattern),
            model=args.model,
            overwrite=args.overwrite,
            out_dir=args.output,
            )
    start_time = datetime.now()
    run(**config)
    end_time = datetime.now()
    within = humanize.naturaldelta(end_time - start_time)
    print(f'Cmorisation finished in {within}')


def run(**kwargs):
    """Apply the standartisation from a given configuration."""

    kwargs.setdefault('run', True)
    kwargs.setdefault('files', None)
    args = kwargs.pop('drstype'), kwargs.pop('rundata'), kwargs.pop('cmortable')
    apply = kwargs.pop('run')
    files = kwargs.pop('files')
    Processor = preproc(*args, **kwargs)
    if apply:
        return Processor.run(files)
    return Processor
if __name__ == '__main__':
    cmd_interface()
