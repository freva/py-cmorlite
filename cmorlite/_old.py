def _correct_metadata(self, fileobj, inputVarName):
        with TemporaryDirectory() as td:
            td = (Path('.')/ 'temp_dir')
            table_dir = Path(td) / 'Tables'
            table_dir.mkdir(exist_ok=True, parents=True)
            d = fileobj(inputVarName)
            lat = d.getLatitude()
            lon = d.getLongitude()
            dims = ' '.join([dim.id for dim in d.getAxisList()])
            #time = d.getTime() ; # Assumes variable is named 'time', for the demo file this is named 'months'
            time = d.getAxis(0) ; # Rather use a file dimension-based load statement
            # Deal with problematic "months since" calendar/time axis
            time_bounds = time.getBounds()
            try:
                time_bounds[:,0] = time[:]
                time_bounds[:-1,1] = time[1:]
                time_bounds[-1,1] = time_bounds[-1,0]+1
                time.setBounds(time_bounds)
                del(time_bounds) ; # Cleanup
            except TypeError:
                pass
            print(time.units)
            print(time.getBounds())
            return
            #%% Initialize and run CMOR
            # For more information see https://cmor.llnl.gov/mydoc_cmor3_api/
            self._insert_def_files(td)
            table_id = f'{self.runinfo["project_id"]}_{self.runinfo["frequency"]}'
            table_file = Path('Tables') / f'{table_id}.json'
            self.cmortable['axis_entry'][lon.id] = self.cmortable['axis_entry']['longitude']
            self.cmortable['axis_entry'][lat.id] = self.cmortable['axis_entry']['latitude']
            self.cmortable['variable_entry'][inputVarName]['dimensions'] = dims
            #self.runinfo['activity_id'] = self.runinfo['project_id']
            t_id = self.runinfo.pop('table_id', table_id)
            self.cmortable['Header']['table_id'] = f'Table {t_id}'
            #self.runinfo['table_id'] = self.runinfo['frequency']
            self.cmor_add(Path(td) / 'cmortable-input.json', self.runinfo)
            self.cmor_add(table_dir / f'{table_id}.json', self.cmortable)
            cmor.setup(inpath=str(td.absolute()), netcdf_file_action=cmor.CMOR_REPLACE_4, logfile='cmorLog.txt')
            cmor.dataset_json('cmortable-input.json')
            cmor.load_table(f'{table_file}')
            cmor.set_cur_dataset_attribute('history',fileobj.history) ; # Force input file attribute as history
            axes = {k: {'table_entry': k,
                         'units': fileobj.axes[k].units} for k in fileobj.axes.keys()}
            axes[lat.id]={'table_entry': lat.id,
                          'units': 'degrees_north',
                          'coord_vals': lat[:],
                          'cell_bounds': lat.getBounds(),}
            axes[lon.id]={'table_entry': lon.id,
                          'units': 'degrees_east',
                          'coord_vals': lon[:],
                          'cell_bounds': lon.getBounds()}
            print(axes[lon.id])
            axisIds = list() ; # Create list of axes
            for axis in axes.values():
                axisId = cmor.axis(**axis)
                axisIds.append(axisId)
            #pdb.set_trace() ; # Debug statement

            # Setup units and create variable to write using cmor - see https://cmor.llnl.gov/mydoc_cmor3_api/#cmor_set_variable_attribute
            try:
                var_entry = self.cmortable['variable_entry'][inputVarName]
            except KeyError:
                var_entry = {'units' : d.units,
                             'long_name' : d.long_name}
            d.units = var_entry['units']
            varid   = cmor.variable(inputVarName,d.units,axisIds,missing_value=d.missing)
            values  = np.array(d[:],np.float32)
            # Append valid_min and valid_max to variable before writing using cmor - see https://cmor.llnl.gov/mydoc_cmor3_api/#cmor_set_variable_attribute
            for attr in ('valid_min', 'valid_max'):
                try:
                    cmor.set_variable_attribute(varid, attr,'f', var_entry[attr])
                except KeyError:
                    pass
            # Prepare variable for writing, then write and close file - see https://cmor.llnl.gov/mydoc_cmor3_api/#cmor_set_variable_attribute
            cmor.set_deflate(varid,1,1,1) ; # shuffle=1,deflate=1,deflate_level=1 - Deflate options compress file data
            cmor.write(varid,values,time_vals=time[:],time_bnds=time.getBounds()) ; # Write variable with time axis

@staticmethod
def cmor_add(jsonfile, data):

        jsonfile.parent.mkdir(exist_ok=True, parents=True)
        with open(jsonfile, 'w') as f:
            json.dump(data, f, indent=3)

def _insert_def_files(self, temp_dir):

    table_dir = (Path(temp_dir) / 'Tables').absolute()
    tmpl_dir = Path(__file__).parent.absolute() / 'cmor'
    keys =  ('_control_vocabulary_file',
             '_AXIS_ENTRY_FILE',
             '_FORMULA_VAR_FILE')
    values = ('CMIP6_CV.json',
              'CMIP6_coordinate.json',
              'CMIP6_formula_terms.json')
    for key, value in zip(keys, values):
        key_file = Path(self.runinfo[key])
        for link_dir in (Path(temp_dir).absolute(), table_dir):
            try:
                (link_dir / value).symlink_to(tmpl_dir / key_file)
            except FileExistsError:
                pass
            self.runinfo[key] = '' #key_file.name


