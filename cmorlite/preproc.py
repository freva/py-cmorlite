"""Collection of methods to apply various pre-processing methods.

Each model input demands different pre-processing strategies, this should be
reflected in the preprocessing defintions for each model.
"""

from datetime import datetime
from difflib import SequenceMatcher
import functools
from getpass import getuser
import itertools
import multiprocessing as mp
import numpy as np
import os
from pathlib import Path
import shlex
from subprocess import run, PIPE
import sys
from tempfile import TemporaryDirectory
import pandas as pd
from netCDF4 import Dataset as nc

from ._cmorize import CMoriser, SCRATCH_DIR
from .main import run_cmd

TABLE_DIR = Path(__file__).parent / 'var_tables'
SCRATCH_DIR = f'/scratch/{getuser()[0]}/{getuser()}'

def _similar(method, methods, thresh=0.75):
    similar = [m for m in methods if SequenceMatcher(None, m, method).ratio() >= thresh]
    if similar:
        return ', '.join(similar)

def preproc(*args, model='remo', **kwargs):
    """Preprocessor wrapper.
    This method will retrun a pre-processing method accoring to a model type

    Parameters:
    ===========
        args : tuple
            drstable, runifo, cmortable for the current run
        model : str, (default: remo)
            the input model name that is processed
        kwargs: additional keyword arguments passed

    Returns:
    ========
        DataProcessor.from_method: Preprocessing method
    """
    return DataProcessor.from_method(model, *args, **kwargs)

class DataProcessor(CMoriser):

    @classmethod
    def from_method(cls, model, drstable, runinfo, cmortable, **kwargs):
        """Create instace of the data Processor from a pre defined method."""
        methods = [m.replace('_proc_','') for m in dir(cls) if m.startswith('_proc')]
        method = f'_proc_{model}'
        try:
            cls._model_process = getattr(cls, method)
        except AttributeError:
            msg = f'no method for {model} defined, definitions are {" ".join(methods)}'
            sim_methods = _similar(model, methods)
            if sim_methods:
                msg = f'no mthod for {model} defined, did you mean {sim_methods}'
            raise AttributeError(msg)
        return cls(drstable, runinfo, cmortable, model, **kwargs)

    def __init__(self, drstable, runinfo, cmortable, model=None,
                overwrite=False, out_dir=None, var_table=None):
        """Inherit from CMoriser."""

        super().__init__(drstable, runinfo, cmortable, out_dir=out_dir)
        self.overwrite = overwrite
        self.timestamp = None
        if var_table is None:
            self.var_table = pd.read_csv(TABLE_DIR / f'{model}.csv', index_col=0)
        elif isinstance(var_table, (str, Path)):
            self.var_table = pd.read_csv(var_table)
        else:
            self.var_table = var_table
        self.chain_cmd = ''
        if self.indexbox:
            self.chain_cmd = f'-selindexbox,{self.indexbox}'

    def process(self, infile):
        """Wrapper that wraps the model output processing function."""

        if self._model_process is None:
            raise ValueError('Register a processing method, or create this class with .from_method')
        calendar = self.runinfo['calendar']
        with TemporaryDirectory(prefix='cdo', dir=SCRATCH_DIR) as td:
            res = self._model_process(infile, td, self.var_table, chain_cmd=self.chain_cmd)
            if not isinstance(res, (tuple, list, set)):
                return [1]
            res = np.array(res)
            if len(res.shape) == 1:
                res = res.reshape(1, res.shape)
            elif len(res.shape) > 2 or res.shape[1] != 2:
                raise ValueError('Return Value wrong, return values')
            out_files = []
            for variable, temp_file in res:
                info = self.var_table.loc[self.var_table.cmorname == variable]
                info = info.loc[info.index[0]]
                out_file = self._get_filename_from_struct(variable,
                                                          self.timestamp,
                                                          temp_file)
                out_file.parent.mkdir(exist_ok=True, parents=True)
                if not out_file.is_file() or self.overwrite:
                    self.correct_file(temp_file, out_file, variable, calendar,
                                      lookup=info)
                out_files.append(out_file)
            return out_files


    def _set_timestamp_from_file(self, file):
        """Get the version number from the latest or first file timestamp."""

        timestamp = datetime.fromtimestamp(file.stat().st_mtime)
        if self.timestamp is None:
            self.timestamp = timestamp
            return file
        self.timestamp = max(timestamp, self.timestamp)
        return file

    def run(self, itterator=None, num_procs=None):
        """Apply a cmorisation function.

        Parameters:
        ===========
            itterator, hashable, default : None
                itterator that is applied to the proccessing pool map. If none
                given (default) the files property of this class is taken.
            num_procs, int, default: None
                number of parallel processes. If None given the number is
                calculated for the cpu count. Set to 1 if for sequential
                application.

        Returns:
        ========
            list : results of the functions
        """

        if self.process is None:
            raise ValueError('No processing method given, register a method or create the instance of the class with .from_method')
        num_procs = num_procs or int(max(.25*mp.cpu_count(), 4))
        itterator = itterator or self.files
        itterator_list = [self._set_timestamp_from_file(i) for i in itterator]
        with mp.Pool(processes=max(num_procs, 1)) as pool:
            itt = pool.map(self.process, itterator_list)
            results = []
            for res in itt:
                results.append(res)
        return [f for f in itertools.chain(*results)]

    def register(self, method, **fixed_kwargs):
        """Register a given method as the new processing method.

        Parameters:
        ===========
            method : py-func
                The method that is registered to be the new processing method
                Note: that this method must take an input file, output file
                and a pandas variable file as argument. It also must return
                the new (cmor) variable name
            **fiexed_kwargs:
                Define keyword arguments that will be passed to the registered
                function
        """

        self._model_process = functools.partial(method, **fixed_kwargs)

    @staticmethod
    def _proc_cclm(cls, infile, outdir, var_table, chain_cmd=''):
        """Process cclm model output."""

        infile, outdir = Path(infile), Path(outdir)
        outfile = Path(outdir) / 'temp_file.nc'
        if not infile.is_file():
            return 0

        #split_cmd = f'cdo -f nc4 selindexbox,46,283,46,277 {infile} {outfile}'
        output = []
        splitfile = outfile.with_suffix('')
        run_cmd(f'cdo -f nc4 splitvar {chain_cmd} {infile} {splitfile}_')
        for file in outdir.rglob('temp_file_*.nc'):
            variable = run_cmd(f'cdo showvar {file}').strip()
            new_file = file.with_suffix('.nc4')
            try:
                table = var_table.loc[var_table.name == variable][['cmorname','mul']]
                cmorname = table['cmorname'].values[0]
                mul = float(table['mul'].values[0])
            except (KeyError, IndexError):
                continue
            _ = run_cmd(f'cdo --cmor chname,{variable},{cmorname} -mulc,{mul} -selindexbox,46,283,46,277 {file} {new_file}')
            output.append((cmorname, new_file))
        if len(output) == 0:
            return 0
        return output

    @staticmethod
    def _proc_dyamond(cls, infile, outdir, var_table, chain_cmd=''):
        """Process Dyamond winter data."""
        import xarray as xr
        infile, outdir = Path(infile), Path(outdir)
        outfile = Path(outdir) / 'temp_file_'
        if not infile.is_file():
            return 0
        #split_cmd = run_cmd(f'cdo -f nc4 splitvar {infile} {outfile}')
        df = xr.open_dataset(infile)
        attrs = df.attrs
        output = []
        for i in ('grid_file_uri', 'uuidOfHGrid', 'number_of_grid_used'):
            try:
                attrs.pop(i)
            except KeyError:
                pass
        df.attrs = attrs
        for variable in df.data_vars:
            attrs = df[variable].attrs
            for attr in ('number_of_grid_in_reference',):
                try:
                    del attrs[attr]
                except KeyError:
                    pass
            attrs['CDI_grid_type'] = 'lonlat'
            df[variable].attrs = attrs
            if variable not in var_table['cmorname'].values:
                continue
            new_file = outdir / f'tmp_{variable}.nc'
            #_ = run_cmd(f'cdo --cmor chname,{variable},{cmorname} {file} {new_file}')
            xr.Dataset({variable:df[variable]}, attrs=df.attrs).to_netcdf(new_file)
            output.append((variable, new_file))
        if len(output) == 0:
            return 0
        return output

    @staticmethod
    def _proc_cosmo_rea(cls, infile, outdir, var_table, chain_cmd=''):
        """Process cosmo-rea model output."""

        infile, outdir = Path(infile), Path(outdir)
        outfile = Path(outdir) / 'temp_file_'
        output = []
        if not infile.is_file():
            return 0
        variable = run_cmd(f'cdo showvar {infile}').strip()
        new_file = Path(outdir) / infile.name
        try:
            table = var_table.loc[var_table.name == variable][['cmorname','mul']]
            cmorname = table['cmorname'].values[0]
            mul = float(table['mul'].values[0])
        except (KeyError, IndexError):
            return 0
        _ = run_cmd(f'cdo -f nc4 chname,{variable},{cmorname} -mulc,{mul} {infile} {new_file}')
        with nc(new_file, 'a') as f:
            f.renameVariable('grid_mapping_1', 'rotated_pole')
            f.variables[cmorname].grid_mapping = 'rotated_pole'
        output.append((cmorname, new_file))
        if len(output) == 0:
            return 0
        return output



    @staticmethod
    def _proc_remo(cls, infile, outdir, var_table, chain_cmd=''):
        """Process remo model output."""

        infile, outdir = Path(infile), Path(outdir)
        outfile = Path(outdir) / 'temp_file_'
        if not infile.is_file():
            return 0
        split_cmd = f'cdo -f nc4 --cmor splitvar {chain_cmd} {infile} {outfile}'
        run_cmd(split_cmd)
        output = []
        for file in outdir.rglob('temp_file_*.nc'):
            variable = run_cmd(f'cdo showvar {file}').strip()
            new_file = file.with_suffix('.nc4')
            try:
                cmorname = var_table.loc[variable]['cmorname']
                output.append((cmorname, new_file))
            except KeyError:
                continue
            _ = run_cmd(f'cdo chname,{variable},{cmorname} {file} {new_file}')
        if len(output) == 0:
            return 0
        return output

