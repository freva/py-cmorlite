"""Class definitions to apply the cemorization."""

from datetime import datetime, timedelta
from getpass import getuser
import json
import pandas as pd
from pathlib import Path
import os
import re
import shlex
from subprocess import run, PIPE
import sys
from tempfile import TemporaryDirectory, NamedTemporaryFile

from jinja2 import Template
from jinja2.exceptions import TemplateSyntaxError
from netCDF4 import num2date, date2num, Dataset
import numpy as np

SCRATCH_DIR = Path('/scratch/') / getuser()[0] / getuser()

from .utils import icon2datetime, units

class CMoriser:
    """Collection of methods to cmorise arbitrary input data."""

    files = None

    def __init__(self, drstable, runinfo, cmortable, out_dir=None, files=None):
        """Create an instance of the Cmorise class using meta dta."""

        self.drstable = drstable
        self.runinfo = runinfo
        self.runinfo.setdefault('indexbox', None)
        self.runinfo.setdefault('timeshift', 0)
        self.runinfo.setdefault('rename_dim', {})
        self.indexbox = self.runinfo.pop('indexbox') or None
        self.rename = self.runinfo.pop('rename_dim')
        self.timeshift = int(self.runinfo.pop('timeshift'))
        self.cmortable = cmortable
        self.out_dir = out_dir or SCRATCH_DIR
        tmpl_dir = Path(__file__).parent.absolute() / 'cmor'
        cv_file = tmpl_dir / self.runinfo['_control_vocabulary_file']
        with cv_file.open() as f:
            self.cv_table = json.load(f)['CV']

    @staticmethod
    def _is_length_dim(inunit):
        """Check if a given unit is a length only dimension."""

        return [dim[1:-1] for dim in inunit.units.dimensionality] == ['length']


    def get_unit_conversion_factor(self, inputunit, outputunit, dt):
        """Get a conversion factor between to units

           Parameters:
           ===========

            inputunit : str
                Input unit that needs to be converted
            outputunit : str
                Target unit to convert input unit to
            dt : datetime.timedelta
                Timedelata object for two consecutive timesteps

            Returns:
            ========
             None : The netCDF4.Dataset object is modified inplace
        """

        unit = [inputunit, outputunit]
        # Try to convert the units to pint.units objects
        for n, uu in enumerate(unit):
            if 'frac' in unit[n] or unit[n] in ('1', '1.'):
                # Non-units assignment
                uu = ''
            elif '%' in unit[n]:
                uu = 'percent'
            else:
                # Pint cannot deal with units like s-1
                # try to identify those units and replace numeric values
                # with a '^' (if numeric characters are present but no
                # power sign is given - like in m s-1 -> m s^-1)
                for i in re.findall("[-\d]+", unit[n]):
                    if not '^' in unit[n] or '**' in unit[n]:
                        uu = uu.replace(i, f'^{i}')
            try:
                unit[n] = units(uu.replace('-^', '^-').replace('--', '-'))
            except Exception as e:
                print(f'Unit conversion failed, input unit {inputunit}, target unit {outputunit}, unit passed to pint: {unit[n]}')
                raise e
        inunit, outunit = unit
        if inunit == outunit:
            # If the units are euqal we do not need to do anything
            return 1
        rho_w = 1000 * units('kg/m^3') # ~ Density of water
        density_unit = units('kg/m**2').units
        if self._is_length_dim(inunit) and outunit.units == density_unit:
            # Most likely this is a water based variable, hence convert
            # a water hight unit to a density unit using the density of water
            return (inunit.to_base_units() * rho_w).magnitude
        if self._is_length_dim(outunit) and inunit.units == density_unit:
            # Same as above but vice versa
            conv = outunit.to_base_units() / rho_w
            return conv.to(inunit).magnitude #Convert from SI to base units
        mass_flux_unit = units('kg/m**2/s').units
        flux = (rho_w / (dt * units('s'))).to_base_units()
        if self._is_length_dim(inunit) and outunit.units == mass_flux_unit:
            # Most likely this is a water based variable that needs to be
            # converted to flux unit by aplying rho_w and the time difference
            return (flux*inunit).to_base_units().magnitude
        if self._is_length_dim(outunit) and inunit.units == mass_flux_unit:
            # Same flux cas as above, just vice versa
            return ((outunit.to_base_units() / flux).to(inunit)).magnitude
        # Do a 'normal' magnitude conversion
        if inunit.units == density_unit and outunit.units == mass_flux_unit:
            # Input is density output mass flux -> divide by time unite
            return 1 / (dt * units('s')).to_base_units().magnitude
        if inunit.units == mass_flux_unit and outunit.units == density_unit:
            # Same as above but vice versa
            return (dt * units('s')).to_base_units().magnitude
        return inunit.to(outunit).magnitude

    def _rename_variables(self, f_obj):

        for old_name, new_name in self.rename.items():
            rename_method = f_obj.renameVariable
            if old_name in f_obj.dimensions:
                rename_method = f_obj.renameDimension
            rename_method(old_name, new_name)


    def correct_file(self, infile, outfile, varname, calendar, lookup=None):
        """Run a first correction of the file.

        Parameters:
        ===========

        infile : str, pathlib.Path
            Input file that needs to be corrected (will be don inplace)
        varname : str
            Name of the input variable
        calendar : str
           Target calendar
        lookup : dict, pd.DataFrame
            Variable information of the stored file
        """
        with Dataset(str(infile), 'a') as f:
            self._rename_variables(f)
            try:
                time_bnds = f.variables['time_bnds']
                tunits = f.variables['time'].units
            except KeyError:
                tunits = 'seconds since 1970-01-01'
            try:
                datetimes = num2date(f['time'][:], f['time'].units)
            except ValueError:
                datetimes = icon2datetime(f['time'][:])
            var_metadata = self.cmortable['variable_entry'][varname]
            unit = lookup.get('units') or f[varnname].units
            inunit = unit.strip('[').strip(']')
            outunit = var_metadata['units']
            long_name = lookup.get('long_name') or f[varname].long_name
            f.variables['time'][:] = date2num(datetimes, tunits, calendar=calendar) + self.timeshift
            f.variables['time'].units = tunits
            f.variables['time'].calendar = calendar
            f.variables['time'].axis = 'T'
            f.variables['time'].long_name = f['time'].out_name = 'time'
            try:
                time_bnds = f.variables['time_bnds']
                try:
                    time_bnds_d = num2date(time_bnds[:], time_bnds.units)
                except AttributeError:
                    try:
                        time_bnds_d = num2date(time_bnds[:], f['time'].units)
                    except Exception as e:
                        print(infile)
                        raise e
                f.variables['time_bnds'][:] = date2num(time_bnds_d, tunits, calendar=calendar)
                f.variables['time_bnds'].units = tunits
                f.variables['time_bnds'].calendar = calendar
            except (KeyError, AttributeError):
                pass
            f.variables[varname].long_name = var_metadata['long_name']
            f.variables[varname].units = outunit
            f.variables[varname].standard_name = var_metadata['standard_name']
            if len(datetimes) > 2:
                dt = (datetimes[1] - datetimes[0]).total_seconds()
            else:
                dt = 1
            mul = self.get_unit_conversion_factor(inunit, outunit, dt)
            f.variables[varname][:] = mul * f.variables[varname][:]
        self.correct_metadata(infile, outfile, varname,
                              long_name=long_name, units=unit)
        with Dataset(str(outfile), 'a') as f:
            for attr, value in self.get_global_attrs().items():
                setattr(f, attr, value)

    def _get_drs_attrs(self, varname, timestamp):
        """Construct all drs attributes."""

        lookup = { k: Template(v).render(**self.runinfo)\
                for (k, v) in self.cv_table['path_facets'].items() }
        lookup['variable'] = varname
        lookup['varname'] = varname,
        lookup['version'] = timestamp.strftime('v%Y%m%d')
        lookup['project'] = lookup['project'].lower()
        return lookup

    def _get_timestamp_from_file(self, inpfile):
        cmd = shlex.split(f'cdo showtimestamp {inpfile}')
        env_extra = os.environ.copy()
        env_extra['PATH'] += f":{Path(sys.exec_prefix) / 'bin'}"
        res = run(cmd, stdout=PIPE, stderr=PIPE, check=True, env=env_extra)
        out = res.stdout.decode('utf-8').strip().split()
        timetamps = ' '.join(out).split(' ')
        dt = timedelta(seconds=self.timeshift)
        ts = [(datetime.fromisoformat(ts)+dt).strftime('%Y%m%d%H%M')\
                for ts in (timetamps[0], timetamps[-1])]
        return '-'.join(ts)

    def _get_filename_from_struct(self, varname, timestamp, inpfile):
        """Construct the output filename from the DRS structure."""

        key = tuple(self.drstable.keys())[0]
        file_dir = self.out_dir / key

        file_name, sub_dir = [], []
        drs_struct = self._get_drs_attrs(varname, timestamp)
        ts = self._get_timestamp_from_file(inpfile)
        for part in self.drstable[key]['parts_file_name']:
            if part == 'time':
                file_name.append(ts)
            else:
                file_name.append(drs_struct[part])
        for part in self.drstable[key]['parts_dir']:
            if part != 'file_name':
                sub_dir.append(drs_struct[part])
        fname = Path('/'.join(sub_dir)) / '_'.join(file_name)
        return file_dir / fname.with_suffix('.nc')


    def get_global_attrs(self):
        """Get all global attributes from the experiment info file."""

        keys = {}
        for key in self.cv_table['required_global_attributes']:
            try:
                keys[key] = self.runinfo[key]
            except KeyError:
                raise KeyError(f'Key : {key} expected to be in run control file.')
        self.cv_table['keys'].setdefault('initialization_method', 1)
        self.cv_table['keys'].setdefault('physics_version', 1)
        global_attrs = {}
        for k, v in self.cv_table['keys'].items():
            try:
                global_attrs[k] = Template(v).render(**self.runinfo)
            except (TemplateSyntaxError, TypeError):
                if isinstance(v, int):
                    global_attrs[k] = v
                    continue
                new_key = Template(v.strip('{{}}').strip('}}')).render(**self.runinfo)
                if isinstance(new_key, str):
                    k1, k2 = new_key.strip().split('[')
                    global_attrs[k] = self.cv_table[k1][k2.strip(']')]
        return global_attrs


    def correct_metadata(self, filename, outfile, inputVarName,
                         long_name='', units=''):
        """Apply metadata correction using cdo cmorlite.

        Parameters:
        ===========

         filename : str, pathlib.Path
            Input filename
         outfile : str, pathlib.Path
            Name of the target output file
         inputVarName : str
            Name of the variable
         long_name : str, (default: None)
            Change the long_name attribute of the filename accordingly
            if None given (default) the long_name attr. from the metadata
            lookup file is taken.
         units : str, (default: None)
            Change the units attribute of the filename accordingly
            if None given (default) the units attr. from the metadata
            lookup file is taken.
        """

        lookup = self.cmortable['variable_entry'][inputVarName]
        long_name = lookup.get('long_name') or long_name
        units = lookup.get('units') or units
        with NamedTemporaryFile(suffix='.txt') as metafile:
            with open(metafile.name, 'w') as f:
                meta_data = dict(
                        name=inputVarName,
                        out_name=inputVarName,
                        long_name=long_name,
                        units=units,
                        factor=1.0,
                        missing_value=1e20
                        )
                meta_string='&parameter '+' '.join([f'{k}={v}' for (k, v) in meta_data.items()])
                f.write(meta_string+' /')
            cmd = f'cdo -O -s -f nc4 -z zip_6 --cmor -cmorlite,{metafile.name} {filename} {outfile}'
            env_extra = os.environ.copy()
            env_extra['PATH'] += f":{Path(sys.exec_prefix) / 'bin'}"
            res = run(shlex.split(cmd), stderr=PIPE, stdout=PIPE,
                      check=True, env=env_extra)

def _import(string, table_name):
    """Import the DRSFile structure definition from freva's file.py."""

    with TemporaryDirectory() as td:
        sys.path.insert(0, td)
        with (Path(td) / 'file.py').open('w') as f:
            f.write(string)
        from file import DRSFile
        sys.path.pop(0)
    try:
        struct =  DRSFile.DRS_STRUCTURE[table_name]
    except KeyError:
        raise KeyError(f'table name not found: {table_name}')
    except NameError:
        raise ValueError('freva module not loaded')
    del DRSFile
    return {table_name:struct}


def get_cmor_structure(table_name, freva_path=None):
    """Get the DRS structure from freva's file.py

    This method tries to import freva's file module from the
    config4freva path, regardless of the python environment.
    To be able to make use of this method make sure the freva module is loaded.

    Parameters:
    ===========

     table_name: str
        The name of the DRS definition in the file module

    Returns:
    ========
    dict : dictionary containing the DRS file structure definitions.
    """

    try:
        freva_path = freva_path or os.environ['PYTHONPATH']
    except KeyError:
        raise ValueError('freva module not loaded?')
    try:
        freva_config = tuple(Path(freva_path).parents)[1]
    except IndexError:
        raise ValueError('freva module not loaded?')
    freva_config = freva_config / 'misc4freva' / 'config4freva' / 'file.py'
    try:
        with freva_config.open() as f:
            cfg = f.readlines()[:]
    except FileNotFoundError:
        raise ValueError('freva module not loaded?')

    cfg  = '\n'.join([line.strip('\n') for line in cfg if 'evaluation_system' not in line])
    return _import(cfg, table_name)

