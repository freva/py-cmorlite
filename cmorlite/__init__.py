# coding: utf-8
"""The cmorlite package.

Copyright (c) 2020, Martin Bergemann
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You should have received a copy of the 3-Clause BSD License along with this
program. If not, see <https://opensource.org/licenses/BSD-3-Clause>

"""

__version__ = "2020.12"

from pathlib import Path
from .main import *
from .main import run
from .preproc import DataProcessor
__all__ = main.__all__


class Standard:
    """Interface for CMOR standardisation."""

    Processor = None
    files = None
    __file__ = __file__
    scratch = scratch

    @staticmethod
    def _get_settings(metadata,
                      file_type,
                      glob_pattern,
                      freva_path,
                      *input_directories):

        rundata, table = read_data_from_runinfo(metadata)
        inp_files = []
        if isinstance(input_directories, (str, Path)):
            input_directories = [input_directories]
        for files in input_directories:
            inp_files += [f for f in Path(files).parent.glob(Path(files).name)]
        return dict(
                     drstype=get_cmor_structure(rundata['_DRS_type'], freva_path),
                     rundata=rundata,
                     cmortable=table,
                     run=False,
                     files=get_filenames(inp_files, file_type, glob_pattern)
                    )

    def register(self, method, **fixed_kwargs):
        """Register a given method as the new processing method.

        Parameters:
        ===========
            method : py-func
                The method that is registered to be the new processing method
                Note: that this method must take an input file, output file
                and a pandas variable file as argument. It also must return
                the new (cmor) variable name
            **fiexed_kwargs:
                Define keyword arguments that will be passed to the registered
                function
        """
        self.Processor.register(method, **fixed_kwargs)


    def run(self, itterator=None, num_procs=None):
        """Apply a cmorisation function.

        Parameters:
        ===========
            itterator, hashable, default : None
                itterator that is applied to the proccessing pool map. If none
                given (default) the files property of this class is taken.
            num_procs, int, default: None
                number of parallel processes. If None given the number is
                calculated for the cpu count. Set to 1 if for sequential
                application.

        Returns:
        ========
            list : results of the functions
        """

        return self.Processor.run(itterator=itterator, num_procs=num_procs)

    def __init__(self,
          metadata,
          *input_directories,
          file_type='.nc',
          glob_pattern='*',
          overwrite=False,
          out_dir=None,
          model=None,
          freva_path='/work/ch1187/regiklim-ces/freva/src',
          **kwargs):

        if self.Processor is None:
            settings = self._get_settings(metadata,
                                         file_type,
                                         glob_pattern,
                                         freva_path,
                                         *input_directories)
            settings['out_dir'] = out_dir
            settings['overwrite']  = overwrite
            self.Processor = DataProcessor(settings['drstype'],
                                           settings['rundata'],
                                           settings['cmortable'],
                                           model=model,
                                           out_dir=out_dir,
                                           overwrite=overwrite,
                                           **kwargs)
            self.files = settings.pop('files')


    @classmethod
    def from_existing_preprocessor(cls,
                               metadata,
                               *input_directories,
                               file_type='.nc',
                               glob_pattern='*',
                               overwrite=False,
                               model='remo',
                               out_dir=None,
                               freva_path='/work/ch1187/regiklim-ces/freva/src',
                               **kwargs):

        """Create the standardisation according to a pre define pre processor.

            This method returns an standartisation ojbect, which can be used
            to standardise data for model ouptput that fits predefined
            preprocessing routines.

            Valid models are: remo

            Parameters:
            ===========
             metdata : str, pathlib.Path
                Path to json of toml meta-data file format
            *iput_directories : colection
                Input director(ies) for finding input files
            file_type : str, (default: '.nc')
                Input file types
            glob_pattern : str (default: '*')
                Glob-pattern to be used when searching for files
            out_dir : str, pathlib.Path (default: None)
                Directory where output should be saved to. If None given (default)
                the users SCRATCH directory is taken.
            overwrite : bool (default: False)
                Overwrite existing standardised file
            model : str (default: remo)
                Model output type that is going to be cmorised
            freva_path: str, pathlib.Path, default: /work/ch1187/regiklim-ces/freva/src
                Source directory of the freva environment

            Returns:
            ========
                Cmorisation object
        """

        settings = cls._get_settings(metadata,
                                     file_type,
                                     glob_pattern,
                                     freva_path,
                                     *input_directories)

        cls.Processor = DataProcessor.from_method(model,
                                                  settings['drstype'],
                                                  settings['rundata'],
                                                  settings['cmortable'],
                                                  out_dir=out_dir)
        cls.files = settings.pop('files')
        return cls(metadata, *input_directories, file_type=file_type,
                   glob_pattern=glob_pattern, overwrite=overwrite,
                   out_dir=out_dir, freva_path=freva_path)

