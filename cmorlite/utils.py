"""Collection of useful methods and functions."""

from datetime import datetime, timedelta
from pathlib import Path
import os
import shlex
from subprocess import run as sp_run, PIPE, CalledProcessError
import sys
import warnings

import pandas as pd
import numpy as np
import toml

from .config_parsers import CmorParser

import pint
import pint.unit

UndefinedUnitError = pint.UndefinedUnitError
DimensionalityError = pint.DimensionalityError

units = pint.UnitRegistry(autoconvert_offset_to_baseunit=True)

# Capture v0.10 NEP 18 warning on first creation
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    units.Quantity([])

# For pint 0.6, this is the best way to define a dimensionless unit. See pint #185
units.define(
    pint.unit.UnitDefinition(
        "percent", "%", (), pint.converters.ScaleConverter(0.01)
    )
)

units.define(
    pint.unit.UnitDefinition(
        "frac", "frac.", (), pint.converters.ScaleConverter(1)
    )
)

units.define(
    pint.unit.UnitDefinition(
        "fract", "fract.", (), pint.converters.ScaleConverter(1)
    )
)

units.define(
    pint.unit.UnitDefinition("1", "1.", (), pint.converters.ScaleConverter(1))
)

# Define commonly encountered units not defined by pint
units.define(
    "degrees_north = degree = degrees_N = degreesN = degree_north = degree_N "
    "= degreeN"
)
units.define(
    "degrees_east = degree = degrees_E = degreesE = degree_east = degree_E = degreeE"
)

# Alias geopotential meters (gpm) to just meters
try:
    units._units["meter"]._aliases = ("metre", "gpm")
    units._units["gpm"] = units._units["meter"]
except AttributeError:
    pass

# Silence UnitStrippedWarning
if hasattr(pint, "UnitStrippedWarning"):
    warnings.simplefilter("ignore", category=pint.UnitStrippedWarning)


def get_filenames(in_dirs, ftype, glob):
    """Create a generator and yield input files.

    Parameters:
    ===========

    in_dir: collection, str, pathlib.Path
        collection of input directories
    ftype: str
        filetype suffix
    glob: str
        seach string that is applied to look for files

    Returns:
    ========
        iterator : filenames
    """

    if ftype:
        # Make sure that ftype starts with '.'
        ftype = "." + ftype.strip(".")
    if isinstance(in_dirs, (str, Path)):
        in_dirs = [Path(in_dirs)]
    if not in_dirs:
        raise ValueError("Not files found")
    for id in in_dirs:
        yield from Path(id).rglob(glob + ftype)


def run_cmd(command, env_extra=[]):
    """Run a command

    Parameters:
    ===========
        command : str
            The command that will be applied
        env_extra : list
            Extra PATH environment

    Returns:
    ========
        subprocess.run : output of the subprocess
    """

    env_extra2 = os.environ.copy()
    env_extra2["PATH"] += f":{Path(sys.exec_prefix) / 'bin'}"
    env_extra2["PATH"] += ":".join(env_extra)
    res = sp_run(
        shlex.split(command),
        check=False,
        stderr=PIPE,
        stdout=PIPE,
        env=env_extra2,
    )
    err, stdout = res.stderr.decode("utf-8"), res.stdout.decode("utf-8")
    try:
        res.check_returncode()
    except Exception as e:
        error_msg = f"The following command failed:\n{command}\nstderr: {err}"
        raise CalledProcessError(res.returncode, error_msg)
    return stdout.strip()


def _update_dict(config, update):
    """Walk through a (nested) dict and update its content form another one."""

    for key, value in update.items():
        if isinstance(value, dict):
            config[key] = _update_dict(config.get(key, {}), value)
        else:
            config[key] = value
    return config


def _merge_meta_data(config):
    """Merge data from a list for configurations."""

    if len(config) == 1:
        return config[0]
    config_merge = {key: value for (key, value) in config[0].items()}
    for nn, conf in enumerate(config):
        config_merge = _update_dict(config_merge, conf)
    return config_merge


def read_cmor_table(table, use_cmor_dir=True):
    """Read meta data form a cmor table.

    This method reads data from a cmor table (in cmor/PRODUCT/PRODUCT_suffix)

    Parameters:
    ===========
    table : str
        table name to be read from cmor metadata directory
        Note: the table should have the structure of PRODUCT_suffix
              (e.g CMIP6_6h) glob pattern for multiple files can also be
              given (e.g. CMIP6_?h)
    use_cmor_dir : bool, (default: False)
        if ture (default) use the system cmor table data
        (cmor/PRODUCT/PRODUCT_suffix). If set to false try reading the file
        directly.
    Returns:
    ========
    dict : parsed meta data table
    """

    table = Path(table)
    if use_cmor_dir:
        cmor_dir = Path(__file__).absolute().parent / "cmor" / table.parent
        table = table.name
    else:
        cmor_dir = table.parent
        table = table.name
    config = [CmorParser.load(t) for t in cmor_dir.rglob(table)]
    if not len(config):
        raise FileNotFoundError(f"No files found for table: {table}")
    return _merge_meta_data(config)


def icon2datetime(icon_dates, start=None):
    """Convert datetime objects in icon format to python datetime objects.

    ::

        time = icon2datetime([20011201.5])

    Parameters:
    ==========

    icon_dates: collection
        Collection of icon date dests

    Returns:
    ========

        dates:  pd.DatetimeIndex
    """

    try:
        icon_dates = icon_dates.values
    except AttributeError:
        pass

    try:
        icon_dates = icon_dates[:]
    except TypeError:
        icon_dates = np.array([icon_dates])

    def convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60**2 * 24
        return datetime.strptime(str(int(date)), "%Y%m%d") + timedelta(
            seconds=int(frac_day.round(0))
        )

    conv = np.vectorize(convert)
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates
    return pd.DatetimeIndex(out).to_pydatetime()
