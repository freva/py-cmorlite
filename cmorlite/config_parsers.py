"""Collection of methods to parse various config files."""

import json
import toml

def _get_key_value_pair(line):

    if  not line.strip() and not line.startswith('!'):
        return None, None
    try:
        key, value = line.strip().split('!')[0].strip().split(':')
    except ValueError:
        return None, None
    return key.strip(), value.strip()


class CmorParser:
    """Collection of methods to parse different cmor config files."""

    @staticmethod
    def _read_serial_data(inpfile):
        """Read data in json or toml format."""

        loader = toml
        if inpfile.suffix.lower() in ('.json', '.jsn'):
            loader = json
        with inpfile.open('r') as f:
            return loader.load(f)

    @classmethod
    def load(cls, table_file):
        """Read data from a various input files.

            Parameters:
            ===========
            table_file : pathlib.Path
                Input file name that contains the cmor meta data

            Returns:
            ========
            dict : parsed meta data
        """

        try:
            return cls._read_serial_data(table_file)
        except (json.decoder.JSONDecodeError,
                toml.decoder.TomlDecodeError, FileNotFoundError):
            try:
                return cls._parse_table_data(table_file)
            except FileNotFoundError:
                raise FileNotFoundError(f'Table not found: {table_file}')

    @staticmethod
    def _parse_header(filetype):
        """Parse header data only."""

        header = {}
        for line in filetype:
            key, value = _get_key_value_pair(line)
            if key == 'expt_id_ok':
                try:
                    header[key] += value.replace("'",'').strip()
                except KeyError:
                    header[key] = value.replace("'",'').strip()
            elif key is not None:
                header[key] = value
            if line.startswith('!='):
                # Hearder data has finished, stop from here.
                return {'Header': header}

    @staticmethod
    def _parse_variables_and_dimensions(filetype):
        """Parse the rest of a given filetype."""

        table_data = {'axis_entry':{}, 'variable_entry':{}}
        out = filetype.read().split('!\n')
        for chunk in out:
            for line in chunk.split('\n'):
                key, value = _get_key_value_pair(line)
                if key is None:
                    continue
                if key in ('axis_entry', 'variable_entry'):
                    dim_name = value
                    dim_type = key
                else:
                    try:
                        table_data[dim_type][dim_name][key] = value
                    except KeyError:
                        try:
                            table_data[dim_type][dim_name] = {}
                        except KeyError:
                            table_data[dim_type] = {dim_name:{}}
        return table_data

    @classmethod
    def _parse_table_data(cls, table_file):
        """Parser to read data from old cmor config file."""

        with table_file.open() as f:
            header = cls._parse_header(f)
            var_and_dims = cls._parse_variables_and_dimensions(f)
        return dict(**header, **var_and_dims)

