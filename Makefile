
# Makefile to install/deploy da-pipeline
#
CONDA_ENV?=cmorlite
DEVELOP?=
PYTHON?=3.11
CHANNEL?=conda-forge

PIP_FLAG:=--develop
ifeq ($(strip $(DEVELOP)),)
	PIP_FLAG:=
endif
all: mamba deploy install

mamba:
	rm -rf bin tmp_conda
	curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
	bin/micromamba create -c conda-forge conda -y -p $(PWD)/tmp_conda

deploy:
	$(MAKE) mamba
	echo Creating environment $(cmorlite)
	$(PWD)/tmp_conda/bin/python setup_conda --name $(CONDA_ENV) --channel $(CHANNEL) --python $(PYTHON) $(PIP_FLAG)
	rm -rf bin tmp_conda

install:
	python3 -m pip install --user .

develop:
	python3 -m pip install --user -e .
