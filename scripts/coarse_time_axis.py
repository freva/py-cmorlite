"""Script to create total precipitation flux."""


import argparse
from pathlib import Path

import xarray as xr
from distributed import Client, wait, progress
from dask_jobqueue import SLURMCluster

from cmorlite._cmorize import get_cmor_structure, SCRATCH_DIR as scratch
import re

import warnings

warnings.filterwarnings('ignore', category=RuntimeWarning)

def setup_cluster(account, tmp_dir, partition='gpu'):
    """Setup a dask distributed client using a slurm cluster."""

    print('Creating Dask-Distributed Cluster')
    cluster = SLURMCluster(memory='500GiB',
                           cores=72,
                           project=account,
                           walltime='08:00:00',
                           queue=partition,
                           local_directory=tmp_dir,
                           job_extra=[f'-J ensembleplotter',
                                  f'-D {tmp_dir}',
                                  f'--begin=now',
                                  f'--output={tmp_dir}/LOG_cluster.%j.o',
                                  f'--output={tmp_dir}/LOG_cluster.%j.o'
                                 ],
                       interface='ib0')
    njobs = 1
    cluster.scale(jobs=njobs)
    nitt, max_itt = 0, 4000
    def _get_num_of_workers(cluster):

        workers = []
        for addr in cluster.scheduler_info['workers'].keys():
            ip = ':'.join(addr.split(':')[:-1])
            if ip not in workers:
                workers.append(ip)
        return len(workers)
    print(f'Waiting for {njobs} workers...', end='', flush=True)
    while (_get_num_of_workers(cluster) < njobs):
        nitt += 1
        time.sleep(15)
        if nitt >= max_itt:
            cluster.close()
            raise ValueError('No workers could be started, aborting')
    print(f' done.')
    dask_client = Client(cluster)
    return dask_client



def parse_args(argv=None):
    """Consturct command line argument parser."""

    ap = argparse.ArgumentParser(prog='create_daily_values.py',
            description="""Create total precipitation flux""",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    ap.add_argument('input', metavar='input', type=Path,
                    help='Input directory where to find files')
    ap.add_argument('--freq', type=str, help='Time freq to be applied',
                    default='1d')
    ap.add_argument('--drs-type', type=str, help='Input DRS type',
                    default='cordex')
    ap.add_argument('--components', type=str, nargs='+',
                   help='Variables that are included',
                   default=None)
    args = ap.parse_args()
    inp = {k: v for (k, v) in args._get_kwargs()}
    return inp.pop('input'), inp.pop('components'), inp


def resample_file(dask_client, varn, file_name, new_name,
                 freq, freq_id, freq_id_long,
                 overwrite=False, method='mean', rename={}):
    method_lookup = {'max': 'Maximum', 'min': 'Minimum', 'mean': 'Mean'}
    if new_name.is_file() and not overwrite:
        return
    new_name.parent.mkdir(exist_ok=True, parents=True)
    kwargs = dict(parallel=True, coords="minimal", data_vars="minimal",
                  compat='override', combine='nested', concat_dim='time',
                  chunks={'time': -1})
    dset = xr.open_mfdataset([file_name], **kwargs)
    attrs = dset.attrs
    attrs['frequency'] = freq_id
    attrs['frequency_id'] = freq_id_long
    var_attrs = {vn:dset[vn].attrs for vn in dset.data_vars}
    if method != 'mean':
        try:
            vl = dict(zip(rename.values(), rename.keys()))[varn]
            var_attrs[vl]['long_name'] = f'{method_lookup[method]} '+var_attrs[vl]['long_name']
            var_attrs[varn]= var_attrs[vl]
        except KeyError:
            pass
    var_attrs[varn]['cell_methods'] = f'time: {method_lookup[method].lower()}'
    resample = getattr(dset.rename(rename).resample({'time':freq}), method)(dim='time')
    try:
        new_name.unlink()
    except FileNotFoundError:
        pass
    resample.attrs = attrs
    for vn in resample.data_vars:
        resample[vn].attrs = var_attrs[vn]
    write = resample.to_netcdf(new_name, compute=False).persist()
    progress(write, notebook=False)
    dask_client.cancel([dset, resample])
    del dset, resample

def get_files(indir, comp, **kwargs):

    indir = Path(indir)
    comp = comp or []
    freq_lookup ={'h': ('hr', 'hourly'),
                  'd':('day', 'daily'),
                  'm':('mon', 'monthly')}
    Max = {'tasmax': [], 'tsmax': [], 'sfcwindmax':[]}
    Min = {'tasmin': [], 'tsmin': []}
    max_var = {'tas': [], 'ts': [], 'sfcwind':[]}
    drs_type= kwargs.get('drs_type', 'cordex')
    file_parts = get_cmor_structure(drs_type)[drs_type]
    freq = kwargs.get('freq', '1d')
    freq_int = re.findall('[-\d]+', freq)[0]
    target_freq = freq_lookup[freq.strip(freq_int)][0]
    freq_id_long = 'sampled'
    da_client = Client()
    if int(freq_int) != 1:
        target_freq = f'{freq_int}{target_freq}'
        freq_id_long = f'sampled {freq_int}'
    freq_id_long = f'{freq_id_long} {freq_lookup[freq.strip(freq_int)][1]}'
    for file in indir.rglob('*.nc'):
        parts = dict(zip(file_parts['parts_file_name'], file.name.split('_')))
        if comp and parts['variable'] not in comp:
            continue
        if parts['variable'] in Max or parts['variable'] in Min:
            continue
        parts_freq = parts['cmor_table']
        freq_part = re.findall("[-\d]+", parts_freq)
        new_file = Path(str(file).replace(parts_freq, target_freq))
        for dtype in (Max, Min, max_var):
            try:
                dtype[parts['variable']].append(file)
            except KeyError:
                pass
        resample_file(da_client,
                      parts['variable'],
                      file,
                      new_file,
                      freq,
                      target_freq,
                      freq_id_long)
        #break
    for var, files in max_var.items():
        for file in files:
            parts = dict(zip(file_parts['parts_file_name'], file.name.split('_')))
            for method, data in zip(('max', 'min'), (Max, Min)):
                new_var = f'{var}{method}'
                try:
                    min_max_files = data[new_var]
                except KeyError:
                    continue
                if not min_max_files:
                    parts_freq = parts['cmor_table']
                    freq_part = re.findall("[-\d]+", parts_freq)
                    new_file = Path(str(file).replace(parts_freq, target_freq).replace(var, new_var))
                    resample_file(da_client,
                                  new_var,
                                  file,
                                  new_file,
                                  freq,
                                  target_freq,
                                  freq_id_long,
                                  method=method,
                                  rename={var: new_var})



if __name__ == '__main__':

    inp_dir, data_vars, settings = parse_args()
    get_files(inp_dir, data_vars, **settings)
