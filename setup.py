#!/usr/bin/env python
import os.path as osp
from pathlib import Path
import re
from setuptools import setup, find_packages
import sys


def get_script_path():
    return osp.dirname(osp.realpath(sys.argv[0]))


def read(*parts):
    return open(osp.join(get_script_path(), *parts)).read()


def find_files(path, parent):
    return [
        str(d.relative_to(parent) / "*")
        for d in Path(path).rglob("*")
        if d.is_dir()
    ]


def find_version(*parts):
    vers_file = read(*parts)
    match = re.search(r'^__version__ = "(\d+.\d+)"', vers_file, re.M)
    if match is not None:
        return match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name="cmorlite",
    version=find_version("cmorlite", "__init__.py"),
    author="German Climate Computing Centre (DKRZ)",
    maintainer="Martin Bergemann",
    description="Easy cimorisation of various model input data",
    long_description=read("README.md"),
    long_description_content_type="text/markdown",
    license="BSD-3-Clause",
    packages=find_packages(),
    install_requires=[
        "cdo",
        "humanize",
        "jinja2",
        "pint",
        "netcdf4",
        "numpy",
        "pandas",
        "pytest",
        "toml",
        "tqdm",
    ],
    extras_require={
        "docs": [
            "sphinx",
            "nbsphinx",
            "recommonmark",
            "ipython",  # For nbsphinx syntax highlighting
            "sphinxcontrib_github_alt",
        ],
        "test": [
            "pytest",
            "pytest-cov",
            "nbval",
            "h5netcdf",
            "testpath",
        ],
    },
    entry_points={
        "console_scripts": [
            "py-cmorlite = cmorlite.main:cmd_interface",
            "py_cmorlite = cmorlite.main:cmd_interface",
        ]
    },
    package_data={
        "": find_files(Path("cmorlite") / "cmor", "cmorlite"),
        "cmorlite.var_tables": ["*"],
    },
    python_requires=">=3.6",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Topic :: Scientific/Engineering :: Data Analysis",
        "Topic :: Scientific/Engineering :: Earth Sciences",
    ],
)
